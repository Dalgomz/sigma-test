$(document).ready(function() {
    var jsonData;
    var keys;

    $.getJSON('https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json', function(data) {
        jsonData = data;
        keys = Object.keys(jsonData);
        var depSelect = ["<option> -- Seleccionar -- </option>"];
        keys.forEach(Element => depSelect.push("<option>"+Element+"</option>"));
        $("#departamento").html(depSelect)
    });

    $("#departamento").change(function() {
        var val = $(this).val();
        var ciudadSelect = [];
        jsonData[val].forEach(Element => ciudadSelect.push("<option>"+Element+"</option>"))
        $("#ciudad").html(ciudadSelect);
    });

    var options = [
        "<option value='test'>item1: test 1</option><option value='test2'>item1: test 2</option>",
        "<option value='test'>item2: test 1</option><option value='test2'>item2: test 2</option>",
        "<option value='test'>item3: test 1</option><option value='test2'>item3: test 2</option>"
    ];

    

});