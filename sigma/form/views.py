from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import messages
from form.models import Contact

def index(request):
    if request.method == 'POST':
        if request.POST.get('name') and request.POST.get('email') and request.POST.get('state') and request.POST.get('city'):
            registro = Contact()
            registro.state = request.POST.get('state')
            registro.city = request.POST.get('city')
            registro.name = request.POST.get('name')
            registro.email = request.POST.get('email')
            registro.save()
            messages.success(request,'Success')
            return render(request,'form/index.html')
        else:
            messages.error(request,'Error')
    else:
        return render(request,'form/index.html')

"""
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")
"""